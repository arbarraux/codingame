#!/bin/bash

echo "Running doxygen..."
cd doxy

for FOLDER in *
do
    cd $FOLDER
    doxygen Doxyfile
    cd ..
done
cd ..
echo "Done"
