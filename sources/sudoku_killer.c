#include <math.h>
#include <stdio.h>
#define L 9


void get_line(int line_id, int *grid, int *line) {
  int i;
  for (i = 0; i < L; ++i) {
    line[i] = grid[line_id * L + i];
  }
}

void get_column(int column_id, int *grid, int *column) {
  int i;
  for (i = 0; i < L; ++i) {
    column[i] = grid[i * L + column_id];
  }
}

void get_square(int line, int column, int *grid, int *square) {
  int i;
  int L_root = sqrt(L);
  for (i = 0; i < L; ++i) {
    square[i] = grid[(line / L_root * L_root + i / L_root) * L +
                     (column / L_root * L_root + i % L_root)];
  }
}
int is_possible(int i_line, int i_column, int *grid, int number) {
  int i;
  int line[L];
  int column[L];
  int square[L];
  get_line(i_line, grid, line);
  get_column(i_column, grid, column);
  get_square(i_line, i_column, grid, square);
  for (i = 0; i < L; ++i) {
    if (line[i] == number || column[i] == number || square[i] == number) {
      return 0;
    }
  }
  return 1;
}

void append_grid(char *line, int *grid, int line_id){
    int i;
    for (i = 0; i < L; ++i) {
        grid[line_id * L + i] = line[i] == '.' ? 0 : line[i] - 48;
    }
}

int char_to_index(char letter){
    return letter - 97 < 0 ? letter - 39 : letter - 97;
}

void cages_index(char *cages, int *index){
    int i = 0;
    int is_letter = 1;
    int letter_index;
    while (cages[i] != '\000') {
        if (is_letter) {
            letter_index = char_to_index(cages[i]);
            is_letter = 0;
            ++i;
        }
        else if (cages[i] != ' ') {
            index[letter_index*2] *= 10;
            index[letter_index*2] += cages[i] - 48;
        }
        else {
             is_letter = 1;
        }
        ++i;
    }
}
int solver(int *grid, char *cages, int *cages_sum, int i) {
  int poss_number;
  int line_id, column_id;
  int cage_index;
  if (i == L * L) {
    return 1;
  }
  line_id = i / L;
  column_id = i % L;
  if (!grid[i]) {
    for (poss_number = 1; poss_number < 10 && cages_sum[char_to_index(cages[i]) * 2] >= poss_number; ++poss_number) {
      if (is_possible(line_id, column_id, grid, poss_number)) {
        grid[i] = poss_number;
        cage_index = char_to_index(cages[i]) * 2;
        if ((cages_sum[cage_index] - poss_number >=
             cages_sum[cage_index + 1] - 1)) {
          cages_sum[cage_index] -= poss_number;
          --cages_sum[cage_index + 1];
          if (solver(grid, cages, cages_sum, i + 1)) {
            return 1; // It's possible to place the number
          }
          cages_sum[cage_index] += poss_number;
          ++cages_sum[cage_index + 1];
        }
      }
      grid[i] = 0;
    }
    return 0; // No possibilities
  }
  return solver(grid, cages, cages_sum, i + 1); // Box different to 0
}

int main() {
  int grid[9][9];
  char boxes[9][9];
  int cages_values[52][2] = {{0, 0}};
  int i, j;
  for (int i = 0; i < 9; i++) {
    char grid_line[10];
    scanf("%s%s", grid_line, boxes[i]);
    fgetc(stdin);
    for (j = 0; j < 9; ++j) {
      ++cages_values[char_to_index(boxes[i][j])][1];
    }
    append_grid(grid_line, *grid, i);
  }
  char cages[251];
  scanf("%[^\n]", cages);
  cages_index(cages, *cages_values);
  solver(*grid, *boxes, *cages_values, 0);
  for (i = 0; i < 9; ++i) {
    for (j = 0; j < 9; ++j) {
      printf("%d", grid[i][j]);
    }
    printf("\n");
  }
}
