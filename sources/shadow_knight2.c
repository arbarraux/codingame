#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void move_warmer(int *prec, int *coo, int *search_zone, int i_axes) {
  if (prec[i_axes] > coo[i_axes]) {
    search_zone[1] = (coo[i_axes] + prec[i_axes]) / 2;
    prec[i_axes] = coo[i_axes];
    coo[i_axes] = search_zone[1];
  } else {
    search_zone[0] = (coo[i_axes] + prec[i_axes] + 1) / 2;
    prec[i_axes] = coo[i_axes];
    coo[i_axes] = search_zone[0];
  }
}

int move_cooler(int prec[], int coo[], int search_zone[][2], int i_axes) {
  if (prec[i_axes] < coo[i_axes]) {
    search_zone[i_axes][1] = (coo[i_axes] + prec[i_axes]) / 2;
  } else {
    search_zone[i_axes][0] = (coo[i_axes] + prec[i_axes] + 1) / 2;
  }
  prec[i_axes] = coo[i_axes];
  i_axes = (i_axes + 1) % 2;
  coo[i_axes] = search_zone[i_axes][1] - coo[i_axes] <
                        (search_zone[i_axes][1] - search_zone[i_axes][0]) / 2
                    ? search_zone[i_axes][0]
                    : search_zone[i_axes][1];

  return i_axes;
}

int main() {
  // width of the building.
  int width;
  // height of the building.
  int height;
  scanf("%d%d", &width, &height);
  // maximum number of turns before game over.
  int N;
  scanf("%d", &N);
  int coo[2];
  int warmest[2];
  int prec[2];
  int i_axes = 0;
  int search_zone[2][2] = {{0, width - 1}, {0, height - 1}};
  scanf("%d%d", &coo[0], &coo[1]);
  prec[0] = coo[0], prec[1] = coo[1];

  // game loop
  while (N) {
    // Current distance to the bomb compared to previous distance (COLDER,
    // WARMER, SAME or UNKNOWN)
    char bomb_dir[11];
    scanf("%s", bomb_dir);
    fprintf(stderr, "x: %d %d\ny: %d %d\n", search_zone[0][0],
            search_zone[0][1], search_zone[1][0], search_zone[1][1]);
    fprintf(stderr, "prec: %d %d\n", prec[0], prec[1]);
    fprintf(stderr, "coo: %d %d\n", coo[0], coo[1]);
    fprintf(stderr, "i_axes: %d", i_axes);
    switch (bomb_dir[0]) {
    case 'U':
      coo[0] = search_zone[0][1] - coo[0] < width / 2 ? 0 : width - 1;
      break;
    case 'W':
      move_warmer(prec, coo, search_zone[i_axes], i_axes);
      break;
    case 'C':
      if (search_zone[(i_axes + 1) % 2][0] !=
          search_zone[(i_axes + 1) % 2][1]) {
        fprintf(stderr, "test");
        i_axes = move_cooler(prec, coo, search_zone, i_axes);
      } else {
        move_warmer(coo, prec, search_zone[i_axes], i_axes);
      }
      break;
    case 'S':
      if (search_zone[i_axes][0] == search_zone[i_axes][1]) {
        i_axes = (i_axes + 1) % 2;
        coo[i_axes] =
            search_zone[i_axes][1] - coo[i_axes] <
                    (search_zone[i_axes][1] - search_zone[i_axes][0]) / 2
                ? search_zone[i_axes][0]
                : search_zone[i_axes][1];
      } else {
        coo[i_axes] = (prec[i_axes] + coo[i_axes] + 1) / 2;
        search_zone[i_axes][0] = coo[i_axes];
        search_zone[i_axes][1] = coo[i_axes];
      }
      break;
    }
    // Write an action using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");

    printf("%d %d\n", coo[0], coo[1]);
    --N;
  }

  return 0;
}
