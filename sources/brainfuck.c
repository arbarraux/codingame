/* [[file:../org/brainfuck.org::*Includes][Includes:1]] */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Includes:1 ends here */

/* [[file:../org/brainfuck.org::*Fonctions][Fonctions:1]] */
int interpret(char *code, int *ptr, int *memory, int memory_size, int *inputs,
              char *output) {
  int i = 0, j = 0, k = 0;
  int t;
  int call_stack = 0;
  while (code[i] != '\000') {
    for (t = 0; t < memory_size; ++t) {
      printf("%d ", memory[t]);
    }
    printf("\n");
    switch (code[i]) {
    case '>':
      ++ptr;
      if (ptr >= memory + memory_size) {
        return 1;
      }
      break;
    case '<':
      --ptr;
      if (ptr < memory) {
        return 1;
      }
      break;
    case '+':
      if (*ptr == 255) {
        return 2;
      }
      ++(*ptr);
      break;
    case '-':
      if (*ptr == 0) {
        return 2;
      }
      --(*ptr);
      break;
    case '.':
      output[k] = *ptr;
      ++k;
      break;
    case ',':
      *ptr = inputs[j];
      ++j;
      break;
    case '[':
      ++call_stack;
      if (!*ptr) {
        ++i;
        while (code[i] != ']' || call_stack > 1) {
          if (code[i] == '[') {
            ++call_stack;
          } else if (code[i] == ']') {
            --call_stack;
            ++i;
          }
        }
        --call_stack;
      }
      break;
    case ']':
      --call_stack;
      if (*ptr) {
        --i;
        while (code[i] != '[' || call_stack < 0) {
          if (code[i] == ']') {
            --call_stack;
          } else if (code[i] == '[') {
            ++call_stack;
          } else if (i == 0) {
            return 3;
          }
          --i;
        }
        ++call_stack;
      }
      break;
    }
    ++i;
  }
  if (call_stack) {
    return 3;
  }
  output[k] = '\000';
  printf("%s\n", output);
  return 0;
}
/* Fonctions:1 ends here */

/* [[file:../org/brainfuck.org::*Main][Main:1]] */
int main() {
  int number_of_line;
  int memory_size;
  int number_of_input;
  int error;
  scanf("%d%d%d", &number_of_line, &memory_size, &number_of_input);
  fgetc(stdin);
  int inputs[number_of_input];
  int *memory = malloc(memory_size * sizeof(int));
  int *ptr = memory;

  char *code = malloc(number_of_line * 500 * sizeof(char));
  char *output = malloc(memory_size * sizeof(char));
  for (int i = 0; i < number_of_line; i++) {
    char line[1025];
    scanf("%[^\n]", line);
    fgetc(stdin);
    strcat(code, line);
  }

  for (int i = 0; i < number_of_input; ++i) {
    scanf("%d", &inputs[i]);
    printf("%d\n", inputs[i]);
  }

  error = interpret(code, ptr, memory, memory_size, inputs, output);

  switch (error) {
  case 1:
    printf("POINTER OUT OF BOUNDS\n");
    break;
  case 2:
    printf("INCORRECT VALUE\n");
    break;
  case 3:
    printf("SYNTAX ERROR\n");
    break;
  }

  free(memory);
  free(code);
  free(output);

  return 0;
}
/* Main:1 ends here */
