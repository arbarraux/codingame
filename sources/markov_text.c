#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int random_seed = 0;

typedef struct Couple{ // Couple for ngram
    struct LinkList *value;
    char key[1001];
} Couple;

typedef struct Link {
    struct Link *next;
    char *value;
} Link;

typedef struct LinkList{
    Link *head;
} LinkList;

LinkList *init(char *value){
    LinkList *liste = malloc(sizeof(LinkList));
    Link *maillon = malloc(sizeof(Link));

    if (liste == NULL || maillon == NULL){
        exit(EXIT_FAILURE);
    }

    maillon->value = value;
    maillon->next = NULL;
    liste->head = maillon;

    return liste;
}

void insert_list(LinkList *liste, char *value){
    Link *new = malloc(sizeof(Link));
    if (liste == NULL || new == NULL){
        exit(EXIT_FAILURE);
    }
    new->value = value;
    new->next = liste->head;
    liste->head = new;
}

int length(LinkList *liste){
    if (liste == NULL){
        exit(EXIT_FAILURE);
    }
    int len = 0;
    Link *maillon = liste->head;
    while (maillon != NULL){
        len++;
        maillon = maillon->next;
    }
    return len;
}

char* value_of(LinkList *liste, int index){
    if (liste == NULL){
        exit(EXIT_FAILURE);
    }
    int maillon_to_pass;
    if (index >= 0){
        maillon_to_pass = length(liste) - index - 1;
    } else {
        maillon_to_pass = -1 * index - 1;
    }
    Link *maillon = liste->head;
    while (maillon_to_pass > 0){
        maillon = maillon->next;
        maillon_to_pass --;
    }
    return maillon->value;
}

void insert_couple(Couple ngram[], char key[], LinkList *value){
    int i = 0;
    while (ngram[i].value != NULL) {
        if (strcmp(ngram[i].key, key) == 0) {
            insert_list(ngram[i].value, value->head->value);
            return;
        }
        ++i;
    }
    strcpy(ngram[i].key, key);
    ngram[i].value = value;
}

int pick_option_index( int num_of_options ) {
    random_seed += 7;
    return random_seed % num_of_options;
}

int strip_str(char sequence[], char output[][1001]){
    /* Take the sequence and cut it in list of words in output */
    int i = 0, j = 0;
    int i_word = 0;

    while (sequence[i] != '\0') {
        if (sequence[i] == ' ') {
            output[i_word][j] = '\0';
            ++i_word;
            j = 0;
        } else {
            output[i_word][j] = sequence[i];
            ++j;
        }
        ++i;
    }
    return i_word + 1;
}

void join(char str1[], char str2[], char output[]){
    int i = 0, j = 0;
    while (str1[i] != '\0') {
        output[i] = str1[i];
        ++i;
    }
    while (str2[j] != '\0'){
        output[i+j] = str2[j];
        ++j;
    }
    output[i+j] = '\0';
}

void n_gram(char text_list[][1001], Couple *ngram, int depth, int length){
    int i, j;
    char temp[1001];
    for (i=0; i<length-depth; ++i) {
        temp[0] = '\0';
        for (j = 0; j<depth; ++j) {
            strcat(temp, text_list[j + i]);
            strcat(temp, " ");
        }
        strcat(temp, "\0");
        insert_couple(ngram, temp, init(text_list[i+depth]));
    }
}


int main()
{
    char text[1001];
    char seed[1001];
    char temp[1001];

    char explode[100][1001];
    char strip_text[100][1001] = {{0}};

    Couple ngram[1001];

    int depth;
    int output_length;
    int i, j;
    int seed_length;
    int nb_words;

    scanf("%[^\n]", text);
    scanf("%d", &depth);
    scanf("%d", &output_length); fgetc(stdin);
    scanf("%[^\n]", seed);

    nb_words = strip_str(text, strip_text);
    seed_length = strip_str(seed, explode);

    n_gram(strip_text, ngram, depth, nb_words);

    printf("%s", seed);
    seed[0] = '\0';
    for (i=seed_length-depth; i < seed_length; ++i){
        strcat(seed, explode[i]);
        strcat(seed, " ");
    }
    while (output_length > seed_length) {
        i = 0;
        while (strcmp(seed, ngram[i].key) != 0){
            ++i;
        }
        int random_number = pick_option_index(length(ngram[i].value));
        printf(" %s", value_of(ngram[i].value, random_number));
        strip_str(seed, explode);
        temp[0] = '\0';
        for (j=1; j < depth; ++j){
            strcat(temp, explode[j]);
            strcat(temp, " ");
        }
        join(temp, value_of(ngram[i].value, random_number), seed);
        strcat(seed, " ");
        --output_length;
    }

    printf("\n");


    return 0;
}
