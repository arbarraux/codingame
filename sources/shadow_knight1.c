#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int main() {
  // width of the building.
  int width, height;
  int max_turn;
  int x, y;
  int i;
  int max_y, min_y = 0;
  int max_x, min_x = 0;
  // height of the building.
  // maximum number of turns before game over.
  scanf("%d%d", &width, &height);
  scanf("%d", &max_turn);
  scanf("%d%d", &x, &y);

  max_y = height - 1;
  max_x = width - 1;
  // game loop
  while (max_turn) {
    // the direction of the bombs from batman's current location (U, UR, R, DR,
    // D, DL, L or UL)
    i = 0;
    char bomb_dir[3];
    scanf("%s", bomb_dir);
    // printf("%d %d, %d %d:\n", min_x, max_x, min_y, max_y);
    while (bomb_dir[i] != '\0') {
      switch (bomb_dir[i]) {
      case 'U':
        max_y = y - 1;
        break;
      case 'D':
        min_y = y + 1;
        break;
      case 'R':
        min_x = x + 1;
        break;
      case 'L':
        max_x = x - 1;
        break;
      default:
        return 1;
      }
      ++i;
    }

    y = (min_y + max_y) / 2;
    x = (min_x + max_x) / 2;
    // Write an action using printf(). DON'T FORGET THE TRAILING \n
    // To debug: fprintf(stderr, "Debug messages...\n");

    // the location of the next window Batman should jump to.
    printf("%d %d\n", x, y);
    --max_turn;
  }

  return 0;
}
