/* [[file:../org/lumen.org::*Includes][Includes:1]] */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
/* Includes:1 ends here */

/* [[file:../org/lumen.org::is_in_room][is_in_room]] */
/**
 * \fn bool is_in_room(int length, int x, int y)
 * \brief Teste si la case existe
 * \param length La longueur de la pièce
 * \param x Abscisse de la case à tester
 * \param y Ordonée de la case à tester
 * \return Renvoie 1 si la case existe */
bool is_in_room(int length, int x, int y) {
  return (x >= 0 && x < length && y >= 0 && y < length);
}
/* is_in_room ends here */

/* [[file:../org/lumen.org::light][light]] */
/**
 * \fn int light(bool **room, int length, int lightning_radius, int x, int y)
 * \brief Eclaire les cases autour de la bougie (x, y)
 * \param room La pièce de départ
 * \param length La longueur de la pièce
 * \param lightning_radius Rayon d'action de la bougie
 * \param x Abscisse de la bougie
 * \param y Ordonée de la bougie
 * \return Renvoie le nombre de nouvelles cases éclairées */
int light(bool **room, int length, int lightning_radius, int x, int y) {
  int inlight_boxes = 0;
  int i, j;
  for (i = -lightning_radius + 1; i < lightning_radius; ++i) {
    for (j = -lightning_radius + 1; j < lightning_radius; ++j) {
      if (is_in_room(length, x + i, y + j) && !room[x + i][y + j]) {
        room[x + i][y + j] = 1;
        ++inlight_boxes;
      }
    }
  }
  return inlight_boxes;
}
/* light ends here */

/* [[file:../org/lumen.org::main][main]] */
int main() {
  int i;
  int N;
  scanf("%d", &N);
  int L;
  scanf("%d", &L);
  bool **room;
  int hiding_places = N * N;

  room = malloc(N * sizeof(*room));
  for (i = 0; i < N; ++i) {
    room[i] = malloc(N * sizeof(bool));
  }

  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      char cell[4];
      scanf("%s", cell);
      if (cell[0] == 'C') {
        hiding_places -= light(room, N, L, i, j);
      }
    }
  }

  for (i = 0; i < N; ++i) {
    free(room[i]);
  }
  free(room);

  printf("%d\n", hiding_places);

  return 0;
}
/* main ends here */
