#include <stdio.h>

void arc_en_ciel(char chaine[255]) {
  int i = 0;
  while (chaine[i] != '\000') {
    printf("\033[3%dm", i % 7);
    printf("%c", chaine[i]);
    ++i;
  }
  printf("\033[0m");
}

int main() {
  char chaine[255];

  scanf("%s", chaine);

  arc_en_ciel(chaine);
  return 0;
}
