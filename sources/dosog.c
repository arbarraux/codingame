int main() {
#include <stdio.h>
#include <stdlib.h>
return 0;
}

int main() {
int max(int *children, int length){
    int i;
    int max = children[0];
    for (i=1; i<length; ++i){
        if(children[i] > max){
            max = children[i];
        }
    }
    return max;
}

int longuest_path(int graph[][100], int graph_len, int start){
    int i;
    int nb_children;
    int children[graph_len];

    for(i=0; i<graph_len; ++i){
        if (graph[start][i] != 0){
            children[i] = longuest_path(graph, graph_len, i);
        } else {
            children[i] = 0;
        }
    }
    return max(children, graph_len) + 1;
}

return 0;
}

int main(){
    int n;
    int i;
    int dwarf;
    int giant;
    scanf("%d", &n); // Number of connections
    int connection[100][100] = {{0}};
    int possible_longuest_path[100] = {0};

    for (i=0; i<n; ++i) {
        scanf("%d", &dwarf); // Connection input "1 3"
        scanf("%d", &giant);
        connection[dwarf - 1][giant - 1] = 1;
    }
    for (i=0; i<n; ++i){
        possible_longuest_path[i] = longuest_path(connection, n, i);
    }
    printf("%d\n", max(possible_longuest_path, n) + 1);
    return 0;
}
