#include <math.h>
#include <stdio.h>
#define L 9

void get_line(int line_id, int *grid, int *line) {
  int i;
  for (i = 0; i < L; ++i) {
    line[i] = grid[line_id * L + i];
  }
}

void get_column(int column_id, int *grid, int *column) {
  int i;
  for (i = 0; i < L; ++i) {
    column[i] = grid[i * L + column_id];
  }
}

void get_square(int line, int column, int *grid, int *square) {
  int i;
  int L_root = sqrt(L);
  for (i = 0; i < L; ++i) {
    square[i] = grid[(line / L_root * L_root + i / L_root) * L +
                     (column / L_root * L_root + i % L_root)];
  }
}
int is_possible(int i_line, int i_column, int *grid, int number) {
  int i;
  int line[L];
  int column[L];
  int square[L];
  get_line(i_line, grid, line);
  get_column(i_column, grid, column);
  get_square(i_line, i_column, grid, square);
  for (i = 0; i < L; ++i) {
    if (line[i] == number || column[i] == number || square[i] == number) {
      return 0;
    }
  }
  return 1;
}

void append_text(char *line, int *grid, int line_id) {
  int i;
  for (i = 0; i < L; ++i) {
    grid[line_id * L + i] = line[i] - 48;
  }
}
int solver(int *grid, int i) {
  int poss_number;
  int line_id, column_id;
  if (i == L * L) {
    return 1;
  }
  line_id = i / L;
  column_id = i % L;
  if (!grid[i]) {
    for (poss_number = 1; poss_number < 10; ++poss_number) {
      if (is_possible(line_id, column_id, grid, poss_number)) {
        grid[i] = poss_number;
        if (solver(grid, i + 1)) {
          return 1; // It's possible to place the number
        }
      }
      grid[i] = 0;
    }
    return 0; // No possibilities
  }
  return solver(grid, i + 1); // Box different to 0
}
int main() {
  int grid[L][L];
  char line[10];
  for (int i = 0; i < 9; i++) {
    scanf("%[^\n]", line);
    fgetc(stdin);
    append_text(line, *grid, i);
  }
  int i, j;
  solver(*grid, 0);
  for (i = 0; i < 9; ++i) {
    for (j = 0; j < 9; ++j) {
      printf("%d", grid[i][j]);
    }
    printf("\n");
  }
  return 0;
}
