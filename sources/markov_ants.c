#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

int search_food(int start[2], char anthill[100][100], int width, int height, int speed){
    int random_dir = rand() % 4;
    int new_y = start[0] + ((random_dir-2)%2) * speed;
    int new_x = start[1] + ((random_dir-1)%2) * speed;
    if (new_x <= 0 || new_x >= width - 1 || new_y <= 0 || new_y >= height - 1){
        return 1;
    } else{
        return 1 + search_food((int[2]) {new_y, new_x}, anthill, width, height, speed);
    }
}

int main()
{
    srand(time(NULL));

    int step;
    scanf("%d", &step);
    int w;
    int h;
    int average = 0;
    int nb_test = 100000;
    scanf("%d%d", &w, &h); fgetc(stdin);
    char anthill[h][w];
    int start[2];
    for (int i = 0; i < h; i++) {
        scanf("%[^\n]", anthill[i]); fgetc(stdin);
        for (int j = 0; j < w; ++j){
            if (anthill[i][j] == 'A'){
                start[0] = i;
                start[1] = j;
            }
        }
    }
    for (int i=0; i<nb_test; ++i) {
        average += search_food(start, anthill, w, h, step);
    }
    printf("%.1f\n", average / (double) nb_test);

    return 0;
}
