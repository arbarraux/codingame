# Ghost legs

![](https://static.tvtropes.org/pmwiki/pub/images/amidarb.jpg)

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table des matières**

1. [1. Présentation du problème](#présentation-du-problème)
    1. [1.1. Exemple](#exemple)
2. [2. Résolution](#résolution)
3. [3. Code](#code)
    1. [3.1. Fonction *index_of*](#fonction-index_of)
    2. [3.2 Fonction *main*](#fonction-main)

<!-- markdown-toc end -->


## 1. Présentation du problème

Le problème est simple, il s'agit de savoir à quel symbole de la ligne du bas est associé chaque symbole de la ligne du haut.  
Pour ce faire , on suite les *"tuyaux"* et lorsque l'on rencontre une barre horizontale, on change de *"tuyaux"* et ainsi de suite jusqu'à la fin.

### 1.1. Exemple

On a l'input suivant:  

```
A  B  C
|  |  |
|--|  |
|  |--|
|  |--|
|  |  |
1  2  3
```

On part de A par exemple, on tombe sur une barre horizontale qui nous amène à 2 puis on retrouve une barre horizontale qui nous amène sur le 3 puis l'on croise une dernière barre qui nous ramène sur le 2. On aura à la fin *A associé à 2*.

## 2. Résolution

L'idée est de créer une tableau associant la ligne des titres *(A B C)* avec leur index *(0 3 6)*. A chaque barre rencontrée, on échange les index des 2 tuyaux concernés. *(Par exemple à la première barre, on aurrait (3 0 6)*  
A la fin, le symbole du haut associé à celui du bas est l'index de l'index de la ligne des titres  
Je m'explique, dans le cas précédant, A est à la position de 0 de la ligne des titres :

``` c
['A', ' ', ' ', 'B', ' ', ' ', 'C']
__^________________________________
  0    1    2    3    4    5    6
```

On cherche alors l'indice du 0 dans la table des index.

``` c
[3, 1, 2, 0, 4, 5, 6]
__________^__________
 0  1  2  3  4  5  6
```
Et enfin en affiche la ligne du bas à l'indice trouvé.

``` c
['1', ' ', ' ', '2', ' ', ' ', '3']
_________________^_________________
  0    1    2    3    4    5    6
```
On affichera donc *A2*

## 3. Code

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int index_of(int a[], int value, int length) {
  int i;
  for (i = 0; i < length; i++) {
    if (a[i] == value) {
      return i;
    }
  }
  return false;
}

int main() {

// +----------------------------------------------------------------
  int W, H;
  int i, j;
  scanf("%d%d", &W, &H); fgetc(stdin);
  char T[W];
  char B[W];
  char line[W];
  int temp;
  int order[W];
// +----------------------------------------------------------------

  // Seting up the array
  for (i = 0; i < W; i++) {
    order[i] = i;
  }

  // Getting the Top line
  scanf("%[^\n]", T); fgetc(stdin);
  
  for (j = 1; j < H - 1; j++) {
    scanf("%[^\n]", line); fgetc(stdin);
    for (i = 0; i < W; i++) {
      if (line[i] == '-') {
        // Swaping the two elem
        temp = order[i - 1];
        order[i - 1] = order[i + 2];
        order[i + 2] = temp;
        i += 3;
      }
    }
  }
  char first_char = T[0];
  // Getting Bottom line
  scanf("%[^\n]", B); fgetc(stdin);
  T[0] = first_char;

  for (i = 0; i < W; i += 3) {
    printf("%c%c\n", T[i], B[index_of(order, i, W)]);
  }

  return 0;
}

```

### 3.1. Fonction *index_of*

| Input                                | Output           |
|:-------------------------------------|:-----------------|
| *int* a[], *int* value, *int* length | *int* i \| false |

Il s'agit simplement de parcourrir le tableau *a* à la recherche de *value* et on renvoie son indice s'il existe. Sinon, on renvoie *false*.

### 3.2 Fonction *main*

On récupère les entiers *W* et *H* soit *W* la longueur des lignes et *H* le nombre de ligne.  
On créé on tableau *(ordre)* d'indice allant de 0 à W-1.
On récupère d'abord la ligne des titres.*(T)*
Ensuite on prend chaque ligne et :

  - On parcours la ligne a la recherche de tiret.  
  - Si l'on trouve un tiret on permute les indices des des tuyaux adjacent dans le tableau *ordre*  
  - On passe les 2 prochaines cases de la ligne pour ce retrouver au prochain tuyaux.  
  
On récupère la ligne des symboles du bas.

Enfin pour chaque symbole du titre, On affiche le symbole, puis le symbole qui lui est associé comme expliqué plus tôt.

## 4. Code de la communauté
Voici la solution de BeeGee, cette solution n'est pas grandement différente de la mienne si ce n'est qu'elle est plus courte.
``` C
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    int W,H;
    scanf("%d%d", &W, &H); fgetc(stdin);
    
    char map[101][101];
    for(int h=0 ; h<H ; h++ ){
        fgets(map[h], 101, stdin);
    }
    for(int h=H-2 ; h>0 ; h-- ){
        for(int w=1 ; w<W ; w+=3 ){
            if( map[h][w]=='-' ){
                char temp=map[H-1][w-1];
                map[H-1][w-1]=map[H-1][w+2];
                map[H-1][w+2]=temp;
            }
        }
    }
    for(int w=0 ; w<W ; w+=3 ){
        printf("%c%c\n",map[0][w],map[H-1][w]);
    }
    
    return 0;
}
```
