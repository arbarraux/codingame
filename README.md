# Codingame

Site de Prép'Isima 1 de présentation des codingames.

## Dépendances
  * doxygen

## Installation

``` sh
git clone https://gitlab.isima.fr/arbarraux/codingame.git codingame
cd codingame
./deploy.sh
```
